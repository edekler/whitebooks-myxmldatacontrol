package nl.wh.generic.datacontrols.hrservice.customization;

import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;

@ElementCustomization(target =
                      "nl.wh.generic.datacontrols.hrservice.HRServiceDC.getDeptEmps.DepartmentEmployeesResponse.Employee")
public class CustomEmployee {

    private static final String AMOUNT = "12300.375";
    
    @TransientAttr
    public String initSalaryAsString(XMLDCElement element) {
        return AMOUNT;
    }        

    @TransientAttr
    public Double initSalaryAsDouble(XMLDCElement element) {
        return Double.valueOf(AMOUNT);
    }        

}
